# CableModemMonitor.Common
The heart of this project is a set of classes to allow you to "scrape" the HTML from your cable modem.

This has been tested against an Arris SB6183, but I suspect it will work against the Arris SB6190 as well.


## Status
Grabs the channel statistics and returns them in a List<>. You may do as you wish 

## Production Information
The product information is grabbed and returned in a ProductInformation class