﻿using System.IO;
using System.Net;

namespace CableModemMonitor.Common.Web
{
    public class CableModemWeb
    {
        public static string Get(string url)
        {
            var myRequest = (HttpWebRequest)WebRequest.Create(url);
            myRequest.Method = "GET";

            var response = myRequest.GetResponse();

            var responseStream = response.GetResponseStream();
            if (responseStream == null)
            {
                return "";
            }
            var responseReader = new StreamReader(responseStream);

            return responseReader.ReadToEnd();
        }
    }
}
