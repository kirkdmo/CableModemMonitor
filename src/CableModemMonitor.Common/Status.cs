﻿using System.Collections.Generic;
using CableModemMonitor.Common.Motorola;
using CableModemMonitor.Common.Web;
using HtmlAgilityPack;

namespace CableModemMonitor.Common
{
    public class Status
    {
        private const string Address = "http://{0}/RgConnect.asp";

        private readonly StatusParser _statusParser;
        public Status(string ipAddress)
        {
            var s = CableModemWeb.Get(string.Format(Address,ipAddress));

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(s);

            _statusParser = new StatusParser(htmlDoc);
        }

        public IEnumerable<DownstreamChannel> GetDownstreamChannels()
        {
            return _statusParser.GetDownstreamChannels();
        }

        public IEnumerable<UpstreamChannel> GetUpstreamChannels()
        {
            return _statusParser.GetUpstreamChannels();
        }
    }
}
