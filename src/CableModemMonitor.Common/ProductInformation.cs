﻿using CableModemMonitor.Common.Motorola;
using CableModemMonitor.Common.Web;
using HtmlAgilityPack;

namespace CableModemMonitor.Common
{
    public class ProductInformation
    {
        private const string Address = "http://{0}/RgSwInfo.asp";

        private readonly InformationParser _informationParser;
        public ProductInformation(string ipAddress)
        {
            var s = CableModemWeb.Get(string.Format(Address, ipAddress));

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(s);


            _informationParser = new InformationParser(htmlDoc);
        }


        public Information GetInformation()
        {
            return _informationParser.GetInformation();
        }
    }
}
