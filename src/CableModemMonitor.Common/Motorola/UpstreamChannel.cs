﻿namespace CableModemMonitor.Common.Motorola
{
    public class UpstreamChannel : IChannel, IUpChannel
    {
        public int Channel { get; set; }
        public string LockStatus { get; set; }
        public double Power { get; set; }
        public int ChannelId { get; set; }
        public int Frequency { get; set; }
        public string ChannelType { get; set; }
        public string SymbolRate { get; set; }

        public override string ToString()
        {
            return $"{Channel}\t{LockStatus}\t{ChannelType}\t{ChannelId}\t{SymbolRate} Ksym/sec\t{Frequency} Hz\t{Power} dBmV";
        }
    }
}
