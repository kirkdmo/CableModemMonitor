﻿using HtmlAgilityPack;

namespace CableModemMonitor.Common.Motorola
{
    public class InformationParser
    {
        private readonly HtmlDocument _htmlDocument;

        public InformationParser(HtmlDocument htmlDocument)
        {
            _htmlDocument = htmlDocument;
        }

        public Information GetInformation()
        {
            var informationTable = _htmlDocument.DocumentNode.SelectSingleNode("//table[1]");
            var count = 1;

            var information = new Information();
            foreach (var selectNode in informationTable.SelectNodes(".//tr/td"))
            {
                switch (count)
                {
                    case 2:
                        information.Specification = selectNode.InnerText.Trim();
                        break;
                    case 4:
                        information.HardwareVersion = int.Parse(selectNode.InnerText.Trim());
                        break;
                    case 6:
                        information.SoftwareVersion = selectNode.InnerText.Trim();
                        break;
                    case 8:
                        information.MacAddress = selectNode.InnerText.Trim();
                        break;
                    case 10:
                        information.SerialNumber = selectNode.InnerText.Trim();
                        break;
                }
                count++;

            }

            return information;
        }

    }
}
