﻿namespace CableModemMonitor.Common.Motorola
{
    public interface IChannel
    {
        int Channel { get; set; }
        string LockStatus { get; set; }
        double Power { get; set; }
        int ChannelId { get; set; }
        int Frequency { get; set; }



    }
}
