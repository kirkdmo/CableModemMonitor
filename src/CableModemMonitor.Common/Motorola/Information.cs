﻿namespace CableModemMonitor.Common.Motorola
{
    public class Information
    {
        public string Specification { get; set; }
        public int HardwareVersion { get; set; }
        public string SoftwareVersion { get; set; }
        public string MacAddress { get; set; }
        public string SerialNumber { get; set; }

        public override string ToString()
        {
            return
                $"Specification: {Specification}\r\nHardwareVersion: {HardwareVersion}\r\nSoftwareVersion: {SoftwareVersion}\r\nMacAddress: {MacAddress}\r\nSerialNumber: {SerialNumber}\r\n";
        }
    }
}
