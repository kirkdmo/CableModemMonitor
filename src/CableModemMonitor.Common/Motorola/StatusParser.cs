﻿using System.Collections.Generic;
using HtmlAgilityPack;

namespace CableModemMonitor.Common.Motorola
{
    public class StatusParser
    {
        private readonly HtmlDocument _htmlDocument;

        public StatusParser(HtmlDocument htmlDocument)
        {
            _htmlDocument = htmlDocument;

        }

        public IEnumerable<DownstreamChannel> GetDownstreamChannels()
        {
            var downStreamTable = _htmlDocument.DocumentNode.SelectSingleNode("//center[1]/table[1]");
            var count = 1;

            var row = 1;
            var column = 0;
            var downStreamChannelList = new List<DownstreamChannel>();
            DownstreamChannel downstreamChannel = null;

            foreach (var node in downStreamTable.SelectNodes(".//tr/td"))
            {


                column++;

                if (row >= 1 && downstreamChannel != null)
                {
                    var text = node.InnerText.Trim().Split(' ')[0];

                    switch (column)
                    {
                        case 1:
                            downstreamChannel.Channel = int.Parse(text);
                            break;
                        case 2:
                            downstreamChannel.LockStatus = text;
                            break;
                        case 3:
                            downstreamChannel.Modulation = text;
                            break;
                        case 4:
                            downstreamChannel.ChannelId = int.Parse(text);
                            break;
                        case 5:
                            downstreamChannel.Frequency = int.Parse(text);
                            break;
                        case 6:
                            downstreamChannel.Power = double.Parse(text);
                            break;
                        case 7:
                            downstreamChannel.Snr = double.Parse(text);
                            break;
                        case 8:
                            downstreamChannel.Corrected = long.Parse(text);
                            break;
                        case 9:
                            downstreamChannel.UnCorrectables = long.Parse(text);
                            break;


                    }

                }



                if (count % 9 == 0)
                {
                    downstreamChannel = new DownstreamChannel();
                    downStreamChannelList.Add(downstreamChannel);
                    row++;
                    column = 0;
                }

                count++;


            }
            // TODO: there has to be a better way to do this
            downStreamChannelList.RemoveAt(downStreamChannelList.Count - 1);
            return downStreamChannelList;

        }


        public IEnumerable<UpstreamChannel> GetUpstreamChannels()
        {
            var upStreamTable = _htmlDocument.DocumentNode.SelectSingleNode("//center[2]/table[1]");
            var count = 1;

            var row = 1;
            var column = 0;
            var upStreamChannelList = new List<UpstreamChannel>();
            UpstreamChannel upstreamChannel = null;

            foreach (var node in upStreamTable.SelectNodes(".//tr/td"))
            {


                column++;

                if (row >= 1 && upstreamChannel != null)
                {
                    var text = node.InnerText.Trim().Split(' ')[0];

                    switch (column)
                    {
                        case 1:
                            upstreamChannel.Channel = int.Parse(text);
                            break;
                        case 2:
                            upstreamChannel.LockStatus = text;
                            break;
                        case 3:
                            upstreamChannel.ChannelType = text;
                            break;
                        case 4:
                            upstreamChannel.ChannelId = int.Parse(text);
                            break;
                        case 5:
                            upstreamChannel.SymbolRate = text;
                            break;
                        case 6:
                            upstreamChannel.Frequency = int.Parse(text);
                            break;
                        case 7:
                            upstreamChannel.Power = double.Parse(text);
                            break;

                    }

                }



                if (count % 7 == 0)
                {
                    upstreamChannel = new UpstreamChannel();
                    upStreamChannelList.Add(upstreamChannel);
                    row++;
                    column = 0;
                }

                count++;


            }
            // TODO: there has to be a better way to do this
            upStreamChannelList.RemoveAt(upStreamChannelList.Count - 1);
            return upStreamChannelList;
        }
    }
}
