﻿namespace CableModemMonitor.Common.Motorola
{
    public class DownstreamChannel : IChannel, IDownChannel
    {
        public int Channel { get; set; }
        public string LockStatus { get; set; }
        public double Power { get; set; }
        public int ChannelId { get; set; }
        public int Frequency { get; set; }
        public string Modulation { get; set; }
        public double Snr { get; set; }
        public long Corrected { get; set; }
        public long UnCorrectables { get; set; }

        public override string ToString()
        {
            return
                $"{Channel}\t{LockStatus}\t{Modulation}\t{ChannelId}\t{Frequency} Hz\t{Power} dBmV\t{Snr} dB\t{Corrected}\t{UnCorrectables}";
        }
    }
}
