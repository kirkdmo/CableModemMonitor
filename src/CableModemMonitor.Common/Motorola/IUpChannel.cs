﻿namespace CableModemMonitor.Common.Motorola
{
    public interface IUpChannel
    {
        string ChannelType { get; set; }
        string SymbolRate { get; set; }
    }
}
