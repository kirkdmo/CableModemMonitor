﻿namespace CableModemMonitor.Common.Motorola
{
    public interface IDownChannel
    {
        string Modulation { get; set; }
        double Snr { get; set; }
        long Corrected { get; set; }
        long UnCorrectables { get; set; }

    }
}
