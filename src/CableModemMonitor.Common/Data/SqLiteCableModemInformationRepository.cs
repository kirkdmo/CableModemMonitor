﻿using System;
using System.IO;
using System.Linq;
using AutoMapper;
using CableModemMonitor.Common.Model;
using CableModemMonitor.Common.Motorola;
using Dapper;

namespace CableModemMonitor.Common.Data
{
    public class SqLiteCableModemInformationRepository: SqLiteBaseRepository, ICableModemInformationRepository
    {
        public CableModemInformation GetCableModemInformation()
        {
            if (!File.Exists(DatabaseFile)) return null;

            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                var cableModemInfo = cnn.Query<CableModemInformation>(
                    @"SELECT * from CableModemInformation").FirstOrDefault();
                return cableModemInfo;

            }
            
        }

        public void SaveCableModemInformation(Information information)
        {
            if (!File.Exists(DatabaseFile))
            {
                CreateDatabase();
            }


            Mapper.Initialize(cfg => cfg.CreateMap<Information, CableModemInformation>() );

            var cableModemInformation = Mapper.Map<CableModemInformation>(information);
            cableModemInformation.CreationDate = DateTime.Now;

            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                cnn.Execute(
                    @"INSERT INTO CableModemInformation
                    (CreationDate, Specification, HardwareVersion, MacAddress, SerialNumber)
                    SELECT @CreationDate, @Specification, @HardwareVersion,
                            @MacAddress, @SerialNumber
                    WHERE NOT EXISTS(SELECT 1 FROM CableModemInformation WHERE SerialNumber = @SerialNumber)", cableModemInformation);
            }

        }

    }
}
