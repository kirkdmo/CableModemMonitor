﻿using CableModemMonitor.Common.Model;
using CableModemMonitor.Common.Motorola;

namespace CableModemMonitor.Common.Data
{
    public interface ICableModemInformationRepository
    {
        CableModemInformation GetCableModemInformation();
        void SaveCableModemInformation(Information information);
    }
}
