﻿using System;
using System.IO;
using System.Linq;
using AutoMapper;
using CableModemMonitor.Common.Model;
using CableModemMonitor.Common.Motorola;
using Dapper;

namespace CableModemMonitor.Common.Data
{
    public class SqLiteDownStreamStatisticsRepository : SqLiteBaseRepository, IDownStreamStatisticsRepository
    {
        public DownStreamStatistics GetDownStreamStatistics(long id)
        {
            if (!File.Exists(DatabaseFile)) return null;

            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                var downStreamStatistics = cnn.Query<DownStreamStatistics>(
                    @"SELECT * from DownStreamStatistics where id = @id", new { id }).FirstOrDefault();
                return downStreamStatistics;

            }
        }

        public void SaveDownStreamStatistics(DownstreamChannel downStreamChannel, CableModemInformation cableModemInformation)
        {
            if (!File.Exists(DatabaseFile))
            {
                CreateDatabase();
            }


            Mapper.Initialize(cfg => cfg.CreateMap<DownstreamChannel, DownStreamStatistics>());

            var downStreamStatistics = Mapper.Map<DownStreamStatistics>(downStreamChannel);
            downStreamStatistics.CableModemId = cableModemInformation.Id;
            downStreamStatistics.CreationDate = DateTime.Now;
            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                cnn.Execute(
                    @"INSERT INTO DownStreamStatistics
                    (CableModemId, CreationDate, Channel, LockStatus, Power, ChannelId, Frequency, Modulation, Snr, Corrected, Uncorrectables)
                    VALUES (@CableModemId, @CreationDate, @Channel, @LockStatus, @Power, @ChannelId, @Frequency, @Modulation, @Snr, @Corrected, @Uncorrectables)",
                    downStreamStatistics);
            }
        }


    }
}
