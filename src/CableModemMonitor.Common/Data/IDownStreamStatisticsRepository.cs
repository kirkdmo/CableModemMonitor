﻿using CableModemMonitor.Common.Model;
using CableModemMonitor.Common.Motorola;

namespace CableModemMonitor.Common.Data
{
    public interface IDownStreamStatisticsRepository
    {
        DownStreamStatistics GetDownStreamStatistics(long id);
        void SaveDownStreamStatistics(DownstreamChannel downStreamChannel, CableModemInformation cableModemInformation);
    }
}
