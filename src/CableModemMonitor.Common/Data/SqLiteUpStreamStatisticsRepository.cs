﻿using System;
using System.IO;
using System.Linq;
using AutoMapper;
using CableModemMonitor.Common.Model;
using CableModemMonitor.Common.Motorola;
using Dapper;

namespace CableModemMonitor.Common.Data
{
    public class SqLiteUpStreamStatisticsRepository : SqLiteBaseRepository, IUpStreamStatisticsRepository
    {
        public UpStreamStatistics GetUpStreamStatistics(long id)
        {
            if (!File.Exists(DatabaseFile)) return null;

            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                var upStreamStatistics = cnn.Query<UpStreamStatistics>(
                    @"SELECT * from UpStreamStatistics where id = @id", new { id }).FirstOrDefault();
                return upStreamStatistics;

            }
        }

        public void SaveUpStreamStatistics(UpstreamChannel upStreamChannel, CableModemInformation cableModemInformation)
        {
            if (!File.Exists(DatabaseFile))
            {
                CreateDatabase();
            }


            Mapper.Initialize(cfg => cfg.CreateMap<UpstreamChannel, UpStreamStatistics>());

            var upStreamStatistics = Mapper.Map<UpStreamStatistics>(upStreamChannel);
            upStreamStatistics.CableModemId = cableModemInformation.Id;
            upStreamStatistics.CreationDate = DateTime.Now;

            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                cnn.Execute(
                    @"INSERT INTO UpStreamStatistics
                    (CableModemId, CreationDate, Channel, LockStatus, Power, ChannelId, Frequency, ChannelType, SymbolRate)
                    VALUES (@CableModemId, @CreationDate, @Channel, @LockStatus, @Power, @ChannelId, @Frequency, @ChannelType, @SymbolRate)",
                    upStreamStatistics);
            }

        }
    }
}
