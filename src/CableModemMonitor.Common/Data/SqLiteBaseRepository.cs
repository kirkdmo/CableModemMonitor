﻿using System;
using System.Data.Entity;
using System.Data.SQLite;
using System.IO;
using Dapper;

namespace CableModemMonitor.Common.Data
{
    public class SqLiteBaseRepository
    {
        public static string DatabaseFile { get; } = Path.Combine(Environment.CurrentDirectory, "CableModemData.dat");

        public static SQLiteConnection SimpleDatabaseConnection()
        {
            return new SQLiteConnection($"Data Source={DatabaseFile}");
        }

        public static void CreateDatabase()
        {
            using (var cnn = SimpleDatabaseConnection())
            {
                cnn.Open();
                cnn.Execute(
                    @"CREATE TABLE CableModemInformation(
	                    Id INTEGER PRIMARY KEY,
	                    CreationDate TEXT,
	                    Specification TEXT,
	                    HardwareVersion INTEGER,
	                    MacAddress TEXT,
	                    SerialNumber TEXT
                    );");

                cnn.Execute(
                    @"CREATE TABLE DownStreamStatistics(
	                        Id INTEGER PRIMARY KEY,
	                        CableModemId INTEGER,
	                        CreationDate TEXT,
	                        Channel INTEGER,
	                        LockStatus TEXT,
	                        Power REAL,
	                        ChannelId INTEGER,
	                        Frequency INTEGER,
	                        Modulation TEXT,
	                        Snr REAL,
	                        Corrected INTEGER,
	                        Uncorrectables INTEGER,
	                        FOREIGN KEY(CableModemId) REFERENCES CableModemInformation(Id)
                    );");

                cnn.Execute(
                    @"CREATE TABLE UpStreamStatistics(
	                    Id INTEGER PRIMARY KEY,
	                    CableModemId INTEGER,
	                    CreationDate INTEGER,
	                    Channel INTEGER,
	                    LockStatus TEXT,
	                    Power REAL,
	                    ChannelId INTEGER,
	                    Frequency INTEGER,
	                    ChannelType TEXT,
	                    SymbolRate TEXT,
	                    FOREIGN KEY (CableModemId) REFERENCES CableModemInformation(Id)

                    );");
            }
        }
    }
}
