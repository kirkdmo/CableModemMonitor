﻿using CableModemMonitor.Common.Model;
using CableModemMonitor.Common.Motorola;

namespace CableModemMonitor.Common.Data
{
    public interface IUpStreamStatisticsRepository
    {
        UpStreamStatistics GetUpStreamStatistics(long id);
        void SaveUpStreamStatistics(UpstreamChannel upStreamChannel, CableModemInformation cableModemInformation);
    }
}
