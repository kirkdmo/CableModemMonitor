﻿using System;

namespace CableModemMonitor.Common.Model
{
    public class UpStreamStatistics
    {
        public int Id { get; set; }
        public int CableModemId { get; set; }
        public DateTime CreationDate { get; set; }
        public int Channel { get; set; }
        public string LockStatus { get; set; }
        public double Power { get; set; }
        public int ChannelId { get; set; }
        public int Frequency { get; set; }
        public string ChannelType { get; set; }
        public string SymbolRate { get; set; }
    }
}
