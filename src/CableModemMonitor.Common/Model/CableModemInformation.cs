﻿using System;

namespace CableModemMonitor.Common.Model
{
    public class CableModemInformation
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Specification { get; set; }
        public int HardwareVersion { get; set; }
        public string SoftwareVersion { get; set; }
        public string MacAddress { get; set; }
        public string SerialNumber { get; set; }
    }
}
