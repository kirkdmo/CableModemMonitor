﻿using System;
using System.Threading;
using CableModemMonitor.Common;
using CableModemMonitor.Common.Data;

namespace CableModemMonitor.Cli
{
    internal class Program
    {
        private static void Main()
        {

            var t = new Timer(TimerCallBack, null, 0, 300000);

            Console.WriteLine("Finished");
            Console.ReadKey();
        }

        private static void TimerCallBack(object sender)
        {
            var information = new ProductInformation("192.168.100.1");
            var productInfo = information.GetInformation();

            var sqlInformation = new SqLiteCableModemInformationRepository();
            sqlInformation.SaveCableModemInformation(productInfo);
            var cableModemInfo = sqlInformation.GetCableModemInformation();
            Console.WriteLine($"-------------{DateTime.Now}------------------");
            Console.WriteLine(productInfo);
            Console.WriteLine("-------------------------------");


            var status = new Status("192.168.100.1");

            var sqlDownStreamStats = new SqLiteDownStreamStatisticsRepository();

            foreach (var downstreamChannel in status.GetDownstreamChannels())
            {
                sqlDownStreamStats.SaveDownStreamStatistics(downstreamChannel, cableModemInfo);
                Console.WriteLine(downstreamChannel);
            }

            Console.WriteLine("--------------------------------");

            var sqlUpStreamStats = new SqLiteUpStreamStatisticsRepository();

            foreach (var upstreamChannel in status.GetUpstreamChannels())
            {
                sqlUpStreamStats.SaveUpStreamStatistics(upstreamChannel, cableModemInfo);
                Console.WriteLine(upstreamChannel);
            }
        }
    }
}
